def func(n):
    assert n > 0

    result = []
    if n % 3 == 0:
        result.append('foo')
    if n % 5 == 0:
        result.append('bar')

    '''.join() - возвращает содержимеое итерируемого объекта в одной строке'''
    return ' '.join(result) if result else str(n)

if __name__ == '__main__':
    assert func(17) == '17'
    assert func(24) == 'foo'
    assert func(25) == 'bar'
    assert func(15) == 'foo bar'